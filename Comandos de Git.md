#Comandos de Git

####`$ git init`

inicializa un proyecto git y crea la carpeta .git

***

####`$ git add <nombre_archivo>`
agrega <nombre_archivo> al area de preparación para ser posteriormente confirmado

####`$ git add .`
agrega todos los archivos al area de preparacion

***

####`$ git commit`
confirma los archivos creando una version de los archivos que pueden ser posteriormente accedida y rebisada. es como marcar en el tiempo un estado de los archivos, en caso de una aplicacion una version utilizable.

si se usa solo abre una aplicacion de notas por defecto para agregar un comentario e indicar que cambios fueron agregados.

si se utiliza `$git commit -m` se agrega el comentario entre ""

####`$ git commit -a -m "comentario a agregar"`
se hace un commit sin hacer un $ git add. solo funciona con archivos que han sido modificado no con archivos nuevos que se vayan a agregar

***

####`$ git status`
indica el estado de los archivos. si han sido creados o modificados, agregados al area de preparación o confirmados

####`$ git diff`
para ver las diferencia o modificaciones que se ha hecho en un archivo

***

####`$ git log`
permite ver el historial de commit al repositorio. si se coloca solo asi sale de forma extendida se puede modificar la forma de mostrar la informacion con los siguientes parametros:

1. --oneline: muestra el titulo del comentario del commit
2. --graph: muestra la estructura de las ramas con codigo ASCII sirve para guiarse cuando se trabaja con ramas
3. --decorate: colorea nombre de las ramas
4. --all: muestra todos los commits se utiliza si se mueve el HEAD a una version antigua del repositorio


####`$ git clone <url o direccion del archivo>`
hace una copia del repositorio en conjunto con el historial git.
lo utilice para copiar un repositorio remoto

####`$ git checkout <hash>`
permite mover el puntero HEAD a un commit guardado con anterioridad
tambien se utiliza para moverse entre las ramas
si se trabaja con tag, se puede reemplazar el hash con tag

####`$ git tag <nombre_tag>`
crea un tag o etiqueta a un commit en particular para servir de referencia entre las versiones

`$git tag`: lista las etiquetas creadas
`$git tag -l "patron_de_busquedad"`: filtra el listado de tags

***

####`$ git branch`
comando para trabajar con ramas, si se escribe asi solo lista las ramas creadas hasta ese momentos. Ramas visibles mas no remotas

`$ git branch <nombre_rama>`: crea una rama nueva
`$ git checkout <nombre_rama>`: mueve el HEAD a la rama, si esta ha sido creada previamente

####`$ git merge <nombre_rama>`
fusiona <nombre_rama> a la rama que estemos ubicados

`$git --merge`: listado de ramas que han sido fusionadas a la rama actual
`$git --no-merged`: listado de ramas que no han sido fusionadas a la rama actual

***

####`$git remote add <nombre_conexion> <http-dirección>`
agrega una conexion a un repositorio remoto

`$git remote -v`: muestra los remotos configurados hasta los momentos

####`$git push <nombre_conexion> <nombre_de_rama>`
publica o empuja <nombre_de_rama> al repositorio remoto <nombre_conexion>

####`$git fetch <nombre_conexion> <nombre_rama>`
hala los cambios del repositorio <nombre_conexion> a la rama <nombre_rama>
estos cambios llegan a una rama oculta llamada <nombre_conexion>/<nombre_rama>, asi que para ver los cambios en la rama local hay q ejecutar el comando $ git merge <nombre_conexion>/<nombre_rama> para que los cambios se fusionen con la rama local

####`$git pull <nombre_conexion> <nombre_rama>`
esto hace un fecth y merge a la rama. es actualizar la rama local en conjunto sin divirlo en 2 pasos como el fetch

***
